<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Candal|Lora" rel="stylesheet">

    <!-- Custom Styling -->
    <link rel="stylesheet" href="../../css/style.css">

    <!-- Admin Styling -->
    <link rel="stylesheet" href="../../css/admin.css">

    <title>Admin Section - Add Article</title>
</head>

<body>
    <header>
        <div class="logo">
            <h1 class="logo-text"><span>C</span>Panel</h1>
        </div>
        <i class="fa fa-bars menu-toggle"></i>
        <ul class="nav">
            <li>
                <a href="#">
                    <i class="fa fa-user"></i> yara kleeb
                    <i class="fa fa-chevron-down" style="font-size: .8em;"></i>
                </a>
                <ul>
                    <li><a href="#" class="logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </header>

    <!-- Admin Page Wrapper -->
    <div class="admin-wrapper">

        <!-- Left Sidebar -->
        <div class="left-sidebar">
            <ul>
                <li><a href="index.html">Manage Article</a></li>
                <li><a href="../video/">Manage video</a></li>
                <li><a href="../post/index.html">Manage Post</a></li>
                <li><a href="../users/index.html">Manage Users</a></li>
            </ul>
        </div>
        <!-- // Left Sidebar -->


        <!-- Admin Content -->
        <div class="admin-content">
            <div class="button-group">
                <a href="index.php" class="btn btn-big">Manage Article</a>
            </div>
            <?php
            require_once "../../db/connect.php";

            $conn = connect_mysql_db(); ?>
            <div class="content">
                <h2 class="page-title">Manage Article</h2>
                <form action="../../db/article/create.php" method="post" id="form">
                    <div>
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="text-input" value="">
                    </div>
                    <div>
                        <label>Body</label>
                        <textarea name="body" id="body" value="" class="ckedit" placeholder="your artical" ></textarea>

                    </div>

                    <div>
                        <label>Author</label>
                        <select name="author" class="text-input">
                            <?php

                            $records = $conn->query("SELECT  `name` FROM `author`");
                            if (!mysqli_num_rows($records)) {
                                echo "No data!!";
                                exit;
                            }
                            while ($data = mysqli_fetch_array($records)) {
                                echo "<option  value='" . $data['name'] . "'>" . $data['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div>
                        <label>Image</label>
                        <input type="file" name="image" class="text-input" id="image">
                    </div>

                    <div>
                        <label>Category</label>
                        <select name="topic" class="text-input">
                            <?php
                            $records = $conn->query("SELECT  `category_name` FROM `category`");
                            if (!mysqli_num_rows($records)) {
                                echo "No data!!";
                                exit;
                            }
                            while ($data = mysqli_fetch_array($records)) {
                                echo "<option  value='" . $data['category_name'] . "'>" . $data['category_name'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-big save"><i class="fa fa-save"></i>save</button>

                    </div>
                </form>

            </div>

        </div>
        <!-- // Admin Content -->

    </div>
    <!-- // Page Wrapper -->



    <!-- JQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

    <!-- Ckeditor -->
    <script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
    <!-- Custom Script -->
    <script src="http://localhost/my-blog/js/scripts.js"></script>
    
</body>

</html>