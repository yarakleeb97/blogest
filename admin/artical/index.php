<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Candal|Lora" rel="stylesheet">

    <!-- Custom Styling -->
    <link rel="stylesheet" href="../../css/style.css">

    <!-- Admin Styling -->
    <link rel="stylesheet" href="../../css/admin.css">

    <title>Control panel</title>
</head>

<body>
    <header>
        <div class="logo">
            <h1 class="logo-text"><span>C</span>panel</h1>
        </div>
        <i class="fa fa-bars menu-toggle"></i>
        <ul class="nav">
            <li>
                <a href="#">
                    <i class="fa fa-user"></i> yara kleeb
                    <i class="fa fa-chevron-down" style="font-size: .8em;"></i>
                </a>
                <ul>
                    <li><a href="#" class="logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </header>

    <!-- Admin Page Wrapper -->
    <div class="admin-wrapper">

        <!-- Left Sidebar -->
        <div class="left-sidebar">
            <ul>
                <li><a href="index.php">Manage Article</a></li>
                <li><a href="../post/index.html">Manage Post</a></li>
                <li><a href="../video/index.html">Manage video</a></li>
                <li><a href="../users/index.html">Manage Users</a></li>
            </ul>
        </div>
        <!-- // Left Sidebar -->


        <!-- Admin Content -->
        <div class="admin-content">
            <div class="button-group">
                <a href="create.php" class="btn btn-big">Add Article</a>
            </div>


            <div class="content">

                <h2 class="page-title">Manage Article</h2>
                <table>
                    <thead>
                        <th>ID</th>
                        <th>Title</th>
                        <th colspan="3"></th>
                    </thead>
                    <tbody>
                        <?php
                        require_once "../../db/connect.php";
                        $conn = connect_mysql_db();
                        $records = $conn->query("SELECT `id`, `title` FROM `article`");
                        if (!mysqli_num_rows($records)) {
                            echo "No data!!";
                            exit;
                        }
                        while ($data = mysqli_fetch_array($records)) { ?>
                            <tr>
                                <td><?php echo $data['id'] ?></td>
                                <td><?php echo $data['title'] ?></td>
                                <td><a href="edit.php?edit=<?php echo $data['id'] ?>" class="edit"><i class="fa fa-edit"></i></a></td>
                                <td><a id='id01' href="../../db/article/delete.php?id=<?php echo $data['id']; ?>" class="delete"><i class="fa fa-trash "></i></a></td>
                                <td><a href="#" class="publish"><i class="fa fa-upload "></a></td>
                            </tr>

                        <?php
                        }
                        ?>

                    </tbody>
                </table>

            </div>

        </div>
        <!-- // Admin Content -->

    </div>
    <!-- // Page Wrapper -->



    <!-- JQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Custom Script -->
    <script src="../../js/scripts.js"></script>
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function() {
            $("a.delete").click(function(e) {
                if (!confirm('Are you sure?')) {
                    e.preventDefault();
                    return false;
                }
                return true;
            });
        });
    </script>
</body>

</html>