$(document).ready(function() {
    ClassicEditor
        .create(document.querySelector('#body'))
        .catch(error => {
            console.error(error);
        });

    CKEDITOR.editorConfig = function(config) {
        config.language = 'es';
        config.uiColor = '#F7B42C';
        config.height = 300;
        config.toolbarCanCollapse = true;

    };
    CKEDITOR.replace('body');
    $('#form').validate({
        ignore: [],
        rules: {
            title: {
                required: true,
                minlength: 3
            },
            body: {
                ckeditor_required: true
            },
            image: {
                required: true,
            },
        },
        messages: {
            title: {
                require: 'Please enter title.',
                minlength: 'please at least 3 character'
            },
            body: {
                required: 'Please enter body. ',
            },
            image: {
                required: 'Please enter image.',
            },
        },
        submitHandler: function(form) {
            form.submit();;
        }

    });
    jQuery.validator.addMethod("ckeditor_required", function(value, element) {
        var editorId = $(element).attr('id');
        var messageLength = CKEDITOR.instances[editorId].getData().replace(/<[^>]*>/gi, '').length;
        return messageLength.length === 0;
    }, "This field is required");
});