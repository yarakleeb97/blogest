<?php

function connect_mysql_db() {
    $db_config = [
        'host' => "127.0.0.1",
        'username' => "root",
        'password' => "",
        'db_name' => "blog"
    ];
    $conn = null;
    try {
        $conn = mysqli_connect(
                $db_config['host'],
                $db_config['username'],
                $db_config['password'],
                $db_config['db_name']
        );
        if (!$conn) {
            throw new Exception('Unable to connect');
        }
    } catch (Exception $ex) {
        die("Faild to connect DB!");
    }
    return $conn;
}
