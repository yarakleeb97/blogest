<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Candal|Lora" rel="stylesheet">

    <!-- Custom Styling -->
    <link rel="stylesheet" href="css/style.css">

    <title>Home</title>
</head>

<body>
    <header>
        <div class="logo">
            <h1 class="logo-text"><span>B</span>logest</h1>
        </div>
        <i class="fa fa-bars menu-toggle"></i>
        <ul class="nav">

            <li>
                <a href="#">
                    <i class="fa fa-user"></i> yara kleeb
                    <i class="fa fa-chevron-down" style="font-size: .8em;"></i>
                </a>
                <ul>

                    <li><a href="#" class="logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </header>

    <!-- Page Wrapper -->
    <div class="page-wrapper">


        <!-- Content -->
        <div class="content clearfix">

            <!-- Main Content -->
            <div class="main-content">

                <?php
                require_once "../db/connect.php";
                $conn = connect_mysql_db();
                $records = $conn->query("SELECT `id`, `title`, `body`, `image`, `id_author`, `create_at` FROM `article`");
                if (!mysqli_num_rows($records)) {
                    echo "No data!!";
                    exit;
                }
                while ($data = mysqli_fetch_array($records)) { ?>
                    <div class="post clearfix">
                        <img src="images/<?php echo $data['image'] ?>" alt="" class="post-image">
                        <div class="post-preview">
                            <h2 name="title"><?php echo $data['title'] ?></h2>
                            <i class="far fa-calendar"> <?php echo $data['create_at'] ?></i>
                            <p class="preview-text">
                                <?php echo substr($data['body'], 0, 250) . "...." ?>
                            </p>
                            <a href="single.php?id=<?php echo $data['id'] ?>" class="btn read-more">Read More</a>
                        </div>
                    </div>
                <?php }
                ?>

            </div>
            <!-- // Main Content -->

            <div class="sidebar">
                <div class="section topics">
                    <h2 class="section-title">Category</h2>
                    <ul>
                        <?php
                        $records = $conn->query("SELECT  `category_name` FROM `category`");
                        while ($data = mysqli_fetch_array($records)) {
                            echo "<li  value='" . $data['category_name'] . "'>" . "<a href='#' >" . $data['category_name'] . "</a></li>";
                        }

                        ?>

                    </ul>
                </div>

            </div>

        </div>
        <!-- // Content -->

    </div>
    <!-- // Page Wrapper -->

    <!-- footer -->
    <div class="footer">
        <div class="footer-bottom">
            &copy; Blogest.com | Designed by Yara Kleeb
        </div>
    </div>
    <!-- // footer -->


    <!-- JQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Slick Carousel -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <!-- Custom Script -->
    <script src="js/scripts.js"></script>

</body>

</html>