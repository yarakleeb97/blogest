<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Candal|Lora" rel="stylesheet">

    <!-- Custom Styling -->
    <link rel="stylesheet" href="css/style.css">

    <title>artical</title>
</head>

<body>
    <!-- Facebook Page Plugin SDK -->
    <?php require_once './single.php'; ?>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=285071545181837&autoLogAppEvents=1">
    </script>

    <header>
        <div class="logo">
            <h1 class="logo-text"><span>B</span>logest</h1>
        </div>
        <i class="fa fa-bars menu-toggle"></i>
        <ul class="nav">
            <li><a href="index.php">Home</a></li>

            <li>
                <a href="#">
                    <i class="fa fa-user"></i> yara kleeb
                    <i class="fa fa-chevron-down" style="font-size: .8em;"></i>
                </a>
                <ul>
                    <li><a href="#" class="logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </header>

    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Content -->
        <div class="content clearfix">

            <!-- Main Content Wrapper -->
            <div class="main-content-wrapper">
                <div class="main-content single">
                    <?php
                    require '../db/connect.php';
                    $conn = connect_mysql_db();
                    if (isset($_GET['id'])) {
                        $id = $_GET['id'];
                        $records = $conn->query("SELECT `id`, `title`, `body`, `image`, `id_author`, `create_at` FROM `article` where`id`=$id");
                        $data = mysqli_fetch_array($records);
                    }
                    ?>
                    <h1 class="post-title" name="title">
                        <?php echo $data['title']; ?></h1>
                    <div class="post-content">
                        <p name="body">
                            <?php  echo $data['body'];?> </p>
                        <i class="far fa-calendar" name="create_at">
                            <?php echo $data['create_at'];?></i>
                    </div>

                </div>
            </div>
            <!-- // Main Content -->

            <!-- Sidebar -->
            <div class="sidebar single">

                <div class="fb-page" data-href="https://web.facebook.com/codingpoets/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://web.facebook.com/codingpoets/" class="fb-xfbml-parse-ignore"><a href="https://web.facebook.com/codingpoets/">Coding Poets</a></blockquote>
                </div>


                <div class="section popular">
                    <h2 class="section-title">Popular</h2>
                    <div class="post clearfix">
                        <img src="images/image_1.png" alt="">
                        <a href="" class="title">
                            <h4>How to overcome your fears</h4>
                        </a>
                    </div>

                </div>


            </div>
            <!-- // Sidebar -->

        </div>
        <!-- // Content -->

    </div>
    <!-- // Page Wrapper -->

    <!-- footer -->
    <div class="footer">
        <div class="footer-content">





            <div class="footer-bottom">
                &copy; Blogest.com | Designed by Yara Kleeb
            </div>
        </div>
        <!-- // footer -->


        <!-- JQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Slick Carousel -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

        <!-- Custom Script -->
        <script src="js/scripts.js"></script>

</body>

</html>